extern crate log;
extern crate time;

use std::fs::{self, File};
use std::path::Path;
use std::io::{self, Write};
use log::{Level, Log, Metadata, Record, SetLoggerError};
use std::sync::Mutex;

pub struct MultiLogger {
    loggers: Vec<Box<Log>>,
}

impl MultiLogger {
    pub fn new(loggers: Vec<Box<Log>>) -> Self {
        Self { loggers }
    }

    pub fn init(loggers: Vec<Box<Log>>, level: Level) -> Result<(), SetLoggerError> {
        log::set_max_level(level.to_level_filter());
        log::set_boxed_logger(Box::new(MultiLogger::new(loggers)))
    }
}

impl log::Log for MultiLogger {
    fn enabled(&self, metadata: &Metadata) -> bool {
        self.loggers.iter().any(|l| l.enabled(metadata))
    }

    fn log(&self, record: &Record) {
        for logger in &self.loggers {
            logger.log(record);
        }
    }

    fn flush(&self) {
        for logger in &self.loggers {
            logger.flush();
        }
    }
}

struct FileLoggerInner(String, Box<File>);
pub struct FileLogger {
    inner: Mutex<FileLoggerInner>,
}

impl FileLogger {
    pub fn new(path: &str) -> io::Result<Self> {
        let file = File::create(path)?;

        Ok(Self {
            inner: Mutex::new(FileLoggerInner(path.into(), Box::new(file))),
        })
    }

    fn recreate(&self) {
        let inner = &mut *self.inner.lock().unwrap();
        let sink = &mut inner.1;

        let path = &inner.0;
        if Path::new(&path).exists() {
            return;
        }

        match fs::OpenOptions::new()
            .read(true)
            .create(true)
            .write(true)
            .append(true)
            .open(&path)
        {
            Ok(file) => *sink = Box::new(file),
            Err(err) => println!("== could not create file: {}", err),
        }
    }
}

impl log::Log for FileLogger {
    fn enabled(&self, _: &Metadata) -> bool {
        true
    }

    fn log(&self, record: &Record) {
        if !self.enabled(record.metadata()) {
            return;
        }

        self.recreate();
        let data = entry(record);
        let _ = writeln!(self.inner.lock().unwrap().1, "{}", data);
    }

    fn flush(&self) {
        let inner = &mut *self.inner.lock().unwrap();
        let sink = &mut inner.1;
        sink.flush().expect("should be able to flush");
        sink.sync_data().expect("final sync");
    }
}

pub struct SinkLogger(Mutex<Box<Write + Send>>);
impl SinkLogger {
    pub fn new<T: Write + Send + 'static>(sink: T) -> Self {
        Self {
            0: Mutex::new(Box::new(sink)),
        }
    }
}

impl log::Log for SinkLogger {
    fn enabled(&self, _: &Metadata) -> bool {
        true
    }

    fn log(&self, record: &Record) {
        if !self.enabled(record.metadata()) {
            return;
        }

        let data = entry(record);
        let _ = writeln!(self.0.lock().unwrap(), "{}", data);
    }

    fn flush(&self) {}
}

pub struct StdoutLogger(SinkLogger);
impl StdoutLogger {
    pub fn new() -> Self {
        Self {
            0: SinkLogger::new(io::stdout()),
        }
    }
}
impl log::Log for StdoutLogger {
    fn enabled(&self, _: &Metadata) -> bool {
        true
    }
    fn log(&self, record: &Record) {
        self.0.log(record)
    }
    fn flush(&self) {}
}

fn entry(record: &Record) -> String {
    format!(
        "{} [{}] {}",
        time::strftime("%Y-%m-%d %H:%M:%S", &time::now())
            .expect("requires a valid timestamp. see strftime"),
        record.level().to_string(),
        record.args()
    )
}
